EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L mylib:TPS2034 U?
U 1 1 5DBAC654
P 5900 3950
AR Path="/5981D798/5983DD90/5DBAC654" Ref="U?"  Part="1" 
AR Path="/5981D798/5983E4FD/5DBAC654" Ref="U?"  Part="1" 
AR Path="/59824968/5983F4B3/5DBAC654" Ref="U?"  Part="1" 
AR Path="/59824968/5983F7D4/5DBAC654" Ref="U?"  Part="1" 
AR Path="/5982788E/5983F4B3/5DBAC654" Ref="U?"  Part="1" 
AR Path="/5982788E/5983F7D4/5DBAC654" Ref="U?"  Part="1" 
AR Path="/59829749/59842ECB/5DBAC654" Ref="U?"  Part="1" 
AR Path="/59829749/59842FE8/5DBAC654" Ref="U?"  Part="1" 
AR Path="/59841F95/59842ECB/5DBAC654" Ref="U?"  Part="1" 
AR Path="/59841F95/59842FE8/5DBAC654" Ref="U?"  Part="1" 
AR Path="/59845D4E/59842ECB/5DBAC654" Ref="U?"  Part="1" 
AR Path="/59845D4E/59842FE8/5DBAC654" Ref="U?"  Part="1" 
AR Path="/59845ED6/59842ECB/5DBAC654" Ref="U?"  Part="1" 
AR Path="/59845ED6/59842FE8/5DBAC654" Ref="U?"  Part="1" 
AR Path="/59846013/59842ECB/5DBAC654" Ref="U?"  Part="1" 
AR Path="/59846013/59842FE8/5DBAC654" Ref="U?"  Part="1" 
AR Path="/5DBAC654" Ref="U?"  Part="1" 
AR Path="/5DBAA80F/5DBAC654" Ref="U?"  Part="1" 
AR Path="/5DBAE4F7/5DBAC654" Ref="U1"  Part="1" 
AR Path="/5DBAE549/5DBAC654" Ref="U2"  Part="1" 
F 0 "U1" H 5900 3600 50  0000 C CNN
F 1 "TPS2034" H 5600 3500 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5900 3700 60  0000 C CNN
F 3 "" H 5900 3700 60  0000 C CNN
	1    5900 3950
	1    0    0    -1  
$EndComp
Text HLabel 7500 3650 2    60   Output ~ 0
OUT
Text HLabel 3800 3850 0    60   Input ~ 0
IN
Text HLabel 3800 4500 0    60   Input ~ 0
EN
$Comp
L power:GND #PWR?
U 1 1 5DBAC65D
P 5300 3650
AR Path="/5981D798/5983DD90/5DBAC65D" Ref="#PWR?"  Part="1" 
AR Path="/5981D798/5983E4FD/5DBAC65D" Ref="#PWR?"  Part="1" 
AR Path="/59824968/5983F4B3/5DBAC65D" Ref="#PWR?"  Part="1" 
AR Path="/59824968/5983F7D4/5DBAC65D" Ref="#PWR?"  Part="1" 
AR Path="/5982788E/5983F4B3/5DBAC65D" Ref="#PWR?"  Part="1" 
AR Path="/5982788E/5983F7D4/5DBAC65D" Ref="#PWR?"  Part="1" 
AR Path="/59829749/59842ECB/5DBAC65D" Ref="#PWR?"  Part="1" 
AR Path="/59829749/59842FE8/5DBAC65D" Ref="#PWR?"  Part="1" 
AR Path="/59841F95/59842ECB/5DBAC65D" Ref="#PWR?"  Part="1" 
AR Path="/59841F95/59842FE8/5DBAC65D" Ref="#PWR?"  Part="1" 
AR Path="/59845D4E/59842ECB/5DBAC65D" Ref="#PWR?"  Part="1" 
AR Path="/59845D4E/59842FE8/5DBAC65D" Ref="#PWR?"  Part="1" 
AR Path="/59845ED6/59842ECB/5DBAC65D" Ref="#PWR?"  Part="1" 
AR Path="/59845ED6/59842FE8/5DBAC65D" Ref="#PWR?"  Part="1" 
AR Path="/59846013/59842ECB/5DBAC65D" Ref="#PWR?"  Part="1" 
AR Path="/59846013/59842FE8/5DBAC65D" Ref="#PWR?"  Part="1" 
AR Path="/5DBAC65D" Ref="#PWR?"  Part="1" 
AR Path="/5DBAA80F/5DBAC65D" Ref="#PWR?"  Part="1" 
AR Path="/5DBAE4F7/5DBAC65D" Ref="#PWR02"  Part="1" 
AR Path="/5DBAE549/5DBAC65D" Ref="#PWR05"  Part="1" 
F 0 "#PWR02" H 5300 3400 50  0001 C CNN
F 1 "GND" H 5300 3500 50  0000 C CNN
F 2 "" H 5300 3650 50  0000 C CNN
F 3 "" H 5300 3650 50  0000 C CNN
	1    5300 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 4050 6500 4050
Wire Wire Line
	6900 3650 6900 3850
Wire Wire Line
	6500 3650 6900 3650
Connection ~ 6900 3650
Wire Wire Line
	6500 3850 6900 3850
Connection ~ 6900 3850
Wire Wire Line
	5300 4050 4900 4050
Wire Wire Line
	4900 3850 4900 4050
Connection ~ 4900 3850
$Comp
L power:GND #PWR?
U 1 1 5DBAC672
P 4500 4150
AR Path="/5981D798/5983DD90/5DBAC672" Ref="#PWR?"  Part="1" 
AR Path="/5981D798/5983E4FD/5DBAC672" Ref="#PWR?"  Part="1" 
AR Path="/59824968/5983F4B3/5DBAC672" Ref="#PWR?"  Part="1" 
AR Path="/59824968/5983F7D4/5DBAC672" Ref="#PWR?"  Part="1" 
AR Path="/5982788E/5983F4B3/5DBAC672" Ref="#PWR?"  Part="1" 
AR Path="/5982788E/5983F7D4/5DBAC672" Ref="#PWR?"  Part="1" 
AR Path="/59829749/59842ECB/5DBAC672" Ref="#PWR?"  Part="1" 
AR Path="/59829749/59842FE8/5DBAC672" Ref="#PWR?"  Part="1" 
AR Path="/59841F95/59842ECB/5DBAC672" Ref="#PWR?"  Part="1" 
AR Path="/59841F95/59842FE8/5DBAC672" Ref="#PWR?"  Part="1" 
AR Path="/59845D4E/59842ECB/5DBAC672" Ref="#PWR?"  Part="1" 
AR Path="/59845D4E/59842FE8/5DBAC672" Ref="#PWR?"  Part="1" 
AR Path="/59845ED6/59842ECB/5DBAC672" Ref="#PWR?"  Part="1" 
AR Path="/59845ED6/59842FE8/5DBAC672" Ref="#PWR?"  Part="1" 
AR Path="/59846013/59842ECB/5DBAC672" Ref="#PWR?"  Part="1" 
AR Path="/59846013/59842FE8/5DBAC672" Ref="#PWR?"  Part="1" 
AR Path="/5DBAC672" Ref="#PWR?"  Part="1" 
AR Path="/5DBAA80F/5DBAC672" Ref="#PWR?"  Part="1" 
AR Path="/5DBAE4F7/5DBAC672" Ref="#PWR01"  Part="1" 
AR Path="/5DBAE549/5DBAC672" Ref="#PWR04"  Part="1" 
F 0 "#PWR01" H 4500 3900 50  0001 C CNN
F 1 "GND" H 4500 4000 50  0000 C CNN
F 2 "" H 4500 4150 50  0000 C CNN
F 3 "" H 4500 4150 50  0000 C CNN
	1    4500 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 4500 5300 4250
Wire Wire Line
	3800 4500 3850 4500
$Comp
L Device:R R?
U 1 1 5DBAC67A
P 4000 4000
AR Path="/5981D798/5983DD90/5DBAC67A" Ref="R?"  Part="1" 
AR Path="/5981D798/5983E4FD/5DBAC67A" Ref="R?"  Part="1" 
AR Path="/59824968/5983F4B3/5DBAC67A" Ref="R?"  Part="1" 
AR Path="/59824968/5983F7D4/5DBAC67A" Ref="R?"  Part="1" 
AR Path="/5982788E/5983F4B3/5DBAC67A" Ref="R?"  Part="1" 
AR Path="/5982788E/5983F7D4/5DBAC67A" Ref="R?"  Part="1" 
AR Path="/59829749/59842ECB/5DBAC67A" Ref="R?"  Part="1" 
AR Path="/59829749/59842FE8/5DBAC67A" Ref="R?"  Part="1" 
AR Path="/59841F95/59842ECB/5DBAC67A" Ref="R?"  Part="1" 
AR Path="/59841F95/59842FE8/5DBAC67A" Ref="R?"  Part="1" 
AR Path="/59845D4E/59842ECB/5DBAC67A" Ref="R?"  Part="1" 
AR Path="/59845D4E/59842FE8/5DBAC67A" Ref="R?"  Part="1" 
AR Path="/59845ED6/59842ECB/5DBAC67A" Ref="R?"  Part="1" 
AR Path="/59845ED6/59842FE8/5DBAC67A" Ref="R?"  Part="1" 
AR Path="/59846013/59842ECB/5DBAC67A" Ref="R?"  Part="1" 
AR Path="/59846013/59842FE8/5DBAC67A" Ref="R?"  Part="1" 
AR Path="/5DBAC67A" Ref="R?"  Part="1" 
AR Path="/5DBAA80F/5DBAC67A" Ref="R?"  Part="1" 
AR Path="/5DBAE4F7/5DBAC67A" Ref="R1"  Part="1" 
AR Path="/5DBAE549/5DBAC67A" Ref="R3"  Part="1" 
F 0 "R1" V 4080 4000 50  0000 C CNN
F 1 "1M" V 4000 4000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3930 4000 50  0000 C CNN
F 3 "" H 4000 4000 50  0000 C CNN
	1    4000 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 4150 4000 4500
Connection ~ 4000 4500
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5DBAC682
P 4250 3850
AR Path="/5981D798/5983DD90/5DBAC682" Ref="#FLG?"  Part="1" 
AR Path="/5981D798/5983E4FD/5DBAC682" Ref="#FLG?"  Part="1" 
AR Path="/59824968/5983F4B3/5DBAC682" Ref="#FLG?"  Part="1" 
AR Path="/59824968/5983F7D4/5DBAC682" Ref="#FLG?"  Part="1" 
AR Path="/5982788E/5983F4B3/5DBAC682" Ref="#FLG?"  Part="1" 
AR Path="/5982788E/5983F7D4/5DBAC682" Ref="#FLG?"  Part="1" 
AR Path="/59829749/59842ECB/5DBAC682" Ref="#FLG?"  Part="1" 
AR Path="/59829749/59842FE8/5DBAC682" Ref="#FLG?"  Part="1" 
AR Path="/59841F95/59842ECB/5DBAC682" Ref="#FLG?"  Part="1" 
AR Path="/59841F95/59842FE8/5DBAC682" Ref="#FLG?"  Part="1" 
AR Path="/59845D4E/59842ECB/5DBAC682" Ref="#FLG?"  Part="1" 
AR Path="/59845D4E/59842FE8/5DBAC682" Ref="#FLG?"  Part="1" 
AR Path="/59845ED6/59842ECB/5DBAC682" Ref="#FLG?"  Part="1" 
AR Path="/59845ED6/59842FE8/5DBAC682" Ref="#FLG?"  Part="1" 
AR Path="/59846013/59842ECB/5DBAC682" Ref="#FLG?"  Part="1" 
AR Path="/59846013/59842FE8/5DBAC682" Ref="#FLG?"  Part="1" 
AR Path="/5DBAC682" Ref="#FLG?"  Part="1" 
AR Path="/5DBAA80F/5DBAC682" Ref="#FLG?"  Part="1" 
AR Path="/5DBAE4F7/5DBAC682" Ref="#FLG01"  Part="1" 
AR Path="/5DBAE549/5DBAC682" Ref="#FLG02"  Part="1" 
F 0 "#FLG01" H 4250 3945 50  0001 C CNN
F 1 "PWR_FLAG" H 4250 4030 50  0000 C CNN
F 2 "" H 4250 3850 50  0000 C CNN
F 3 "" H 4250 3850 50  0000 C CNN
	1    4250 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5DBAC688
P 7200 3800
AR Path="/5981D798/5983DD90/5DBAC688" Ref="C?"  Part="1" 
AR Path="/5981D798/5983E4FD/5DBAC688" Ref="C?"  Part="1" 
AR Path="/59824968/5983F4B3/5DBAC688" Ref="C?"  Part="1" 
AR Path="/59824968/5983F7D4/5DBAC688" Ref="C?"  Part="1" 
AR Path="/5982788E/5983F4B3/5DBAC688" Ref="C?"  Part="1" 
AR Path="/5982788E/5983F7D4/5DBAC688" Ref="C?"  Part="1" 
AR Path="/59829749/59842ECB/5DBAC688" Ref="C?"  Part="1" 
AR Path="/59829749/59842FE8/5DBAC688" Ref="C?"  Part="1" 
AR Path="/59841F95/59842ECB/5DBAC688" Ref="C?"  Part="1" 
AR Path="/59841F95/59842FE8/5DBAC688" Ref="C?"  Part="1" 
AR Path="/59845D4E/59842ECB/5DBAC688" Ref="C?"  Part="1" 
AR Path="/59845D4E/59842FE8/5DBAC688" Ref="C?"  Part="1" 
AR Path="/59845ED6/59842ECB/5DBAC688" Ref="C?"  Part="1" 
AR Path="/59845ED6/59842FE8/5DBAC688" Ref="C?"  Part="1" 
AR Path="/59846013/59842ECB/5DBAC688" Ref="C?"  Part="1" 
AR Path="/59846013/59842FE8/5DBAC688" Ref="C?"  Part="1" 
AR Path="/5DBAC688" Ref="C?"  Part="1" 
AR Path="/5DBAA80F/5DBAC688" Ref="C?"  Part="1" 
AR Path="/5DBAE4F7/5DBAC688" Ref="C2"  Part="1" 
AR Path="/5DBAE549/5DBAC688" Ref="C4"  Part="1" 
F 0 "C2" H 7225 3900 50  0000 L CNN
F 1 "0.1uF" H 7225 3700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7238 3650 50  0000 C CNN
F 3 "" H 7200 3800 50  0000 C CNN
	1    7200 3800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DBAC68E
P 7200 3950
AR Path="/5981D798/5983DD90/5DBAC68E" Ref="#PWR?"  Part="1" 
AR Path="/5981D798/5983E4FD/5DBAC68E" Ref="#PWR?"  Part="1" 
AR Path="/59824968/5983F4B3/5DBAC68E" Ref="#PWR?"  Part="1" 
AR Path="/59824968/5983F7D4/5DBAC68E" Ref="#PWR?"  Part="1" 
AR Path="/5982788E/5983F4B3/5DBAC68E" Ref="#PWR?"  Part="1" 
AR Path="/5982788E/5983F7D4/5DBAC68E" Ref="#PWR?"  Part="1" 
AR Path="/59829749/59842ECB/5DBAC68E" Ref="#PWR?"  Part="1" 
AR Path="/59829749/59842FE8/5DBAC68E" Ref="#PWR?"  Part="1" 
AR Path="/59841F95/59842ECB/5DBAC68E" Ref="#PWR?"  Part="1" 
AR Path="/59841F95/59842FE8/5DBAC68E" Ref="#PWR?"  Part="1" 
AR Path="/59845D4E/59842ECB/5DBAC68E" Ref="#PWR?"  Part="1" 
AR Path="/59845D4E/59842FE8/5DBAC68E" Ref="#PWR?"  Part="1" 
AR Path="/59845ED6/59842ECB/5DBAC68E" Ref="#PWR?"  Part="1" 
AR Path="/59845ED6/59842FE8/5DBAC68E" Ref="#PWR?"  Part="1" 
AR Path="/59846013/59842ECB/5DBAC68E" Ref="#PWR?"  Part="1" 
AR Path="/59846013/59842FE8/5DBAC68E" Ref="#PWR?"  Part="1" 
AR Path="/5DBAC68E" Ref="#PWR?"  Part="1" 
AR Path="/5DBAA80F/5DBAC68E" Ref="#PWR?"  Part="1" 
AR Path="/5DBAE4F7/5DBAC68E" Ref="#PWR03"  Part="1" 
AR Path="/5DBAE549/5DBAC68E" Ref="#PWR06"  Part="1" 
F 0 "#PWR03" H 7200 3700 50  0001 C CNN
F 1 "GND" H 7200 3800 50  0000 C CNN
F 2 "" H 7200 3950 50  0000 C CNN
F 3 "" H 7200 3950 50  0000 C CNN
	1    7200 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DBAC694
P 6850 4550
AR Path="/5981D798/5983DD90/5DBAC694" Ref="R?"  Part="1" 
AR Path="/5981D798/5983E4FD/5DBAC694" Ref="R?"  Part="1" 
AR Path="/59824968/5983F4B3/5DBAC694" Ref="R?"  Part="1" 
AR Path="/59824968/5983F7D4/5DBAC694" Ref="R?"  Part="1" 
AR Path="/5982788E/5983F4B3/5DBAC694" Ref="R?"  Part="1" 
AR Path="/5982788E/5983F7D4/5DBAC694" Ref="R?"  Part="1" 
AR Path="/59829749/59842ECB/5DBAC694" Ref="R?"  Part="1" 
AR Path="/59829749/59842FE8/5DBAC694" Ref="R?"  Part="1" 
AR Path="/59841F95/59842ECB/5DBAC694" Ref="R?"  Part="1" 
AR Path="/59841F95/59842FE8/5DBAC694" Ref="R?"  Part="1" 
AR Path="/59845D4E/59842ECB/5DBAC694" Ref="R?"  Part="1" 
AR Path="/59845D4E/59842FE8/5DBAC694" Ref="R?"  Part="1" 
AR Path="/59845ED6/59842ECB/5DBAC694" Ref="R?"  Part="1" 
AR Path="/59845ED6/59842FE8/5DBAC694" Ref="R?"  Part="1" 
AR Path="/59846013/59842ECB/5DBAC694" Ref="R?"  Part="1" 
AR Path="/59846013/59842FE8/5DBAC694" Ref="R?"  Part="1" 
AR Path="/5DBAC694" Ref="R?"  Part="1" 
AR Path="/5DBAA80F/5DBAC694" Ref="R?"  Part="1" 
AR Path="/5DBAE4F7/5DBAC694" Ref="R2"  Part="1" 
AR Path="/5DBAE549/5DBAC694" Ref="R4"  Part="1" 
F 0 "R2" V 6930 4550 50  0000 C CNN
F 1 "10k" V 6850 4550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6780 4550 50  0000 C CNN
F 3 "" H 6850 4550 50  0000 C CNN
	1    6850 4550
	1    0    0    -1  
$EndComp
Text HLabel 7500 4250 2    60   Output ~ 0
/OC
Wire Wire Line
	4900 4700 6850 4700
Connection ~ 4900 4050
Wire Wire Line
	6850 4400 6850 4250
Wire Wire Line
	6500 4250 6850 4250
Connection ~ 6850 4250
Wire Wire Line
	6900 3850 6900 4050
Wire Wire Line
	4900 3850 5300 3850
Wire Wire Line
	4900 4050 4900 4700
Wire Wire Line
	6850 4250 7500 4250
Wire Wire Line
	6900 3650 7200 3650
Wire Wire Line
	3800 3850 3850 3850
Connection ~ 4000 3850
Wire Wire Line
	4000 3850 4250 3850
Connection ~ 4250 3850
Wire Wire Line
	4250 3850 4500 3850
Connection ~ 4500 3850
Wire Wire Line
	4500 3850 4900 3850
Connection ~ 7200 3650
Wire Wire Line
	7200 3650 7400 3650
$Comp
L Connector:Conn_01x03_Male J?
U 1 1 5DBAC6AE
P 3550 3500
AR Path="/5DBAC6AE" Ref="J?"  Part="1" 
AR Path="/5DBAA80F/5DBAC6AE" Ref="J?"  Part="1" 
AR Path="/5DBAE4F7/5DBAC6AE" Ref="J1"  Part="1" 
AR Path="/5DBAE549/5DBAC6AE" Ref="J4"  Part="1" 
F 0 "J1" H 3658 3781 50  0000 C CNN
F 1 "Conn_01x03_Male" H 3658 3690 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3550 3500 50  0001 C CNN
F 3 "~" H 3550 3500 50  0001 C CNN
	1    3550 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J?
U 1 1 5DBAC6B4
P 3650 5000
AR Path="/5DBAC6B4" Ref="J?"  Part="1" 
AR Path="/5DBAA80F/5DBAC6B4" Ref="J?"  Part="1" 
AR Path="/5DBAE4F7/5DBAC6B4" Ref="J2"  Part="1" 
AR Path="/5DBAE549/5DBAC6B4" Ref="J5"  Part="1" 
F 0 "J2" H 3758 5281 50  0000 C CNN
F 1 "Conn_01x03_Male" H 3758 5190 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3650 5000 50  0001 C CNN
F 3 "~" H 3650 5000 50  0001 C CNN
	1    3650 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 4900 3850 4500
Wire Wire Line
	3850 5100 3850 5000
Wire Wire Line
	3850 5000 3850 4900
Connection ~ 3850 5000
Connection ~ 3850 4900
Connection ~ 3850 4500
Wire Wire Line
	3850 4500 4000 4500
Wire Wire Line
	4000 4500 5300 4500
Wire Wire Line
	3850 3850 3850 3600
Wire Wire Line
	3850 3400 3750 3400
Connection ~ 3850 3850
Wire Wire Line
	3850 3850 4000 3850
Wire Wire Line
	3750 3500 3850 3500
Connection ~ 3850 3500
Wire Wire Line
	3850 3500 3850 3400
Wire Wire Line
	3750 3600 3850 3600
Connection ~ 3850 3600
Wire Wire Line
	3850 3600 3850 3500
$Comp
L Connector:Conn_01x03_Male J?
U 1 1 5DBACE1B
P 7200 3350
AR Path="/5DBACE1B" Ref="J?"  Part="1" 
AR Path="/5DBAA80F/5DBACE1B" Ref="J?"  Part="1" 
AR Path="/5DBAE4F7/5DBACE1B" Ref="J3"  Part="1" 
AR Path="/5DBAE549/5DBACE1B" Ref="J6"  Part="1" 
F 0 "J3" H 7308 3631 50  0000 C CNN
F 1 "Conn_01x03_Male" H 7308 3540 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 7200 3350 50  0001 C CNN
F 3 "~" H 7200 3350 50  0001 C CNN
	1    7200 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 3650 7400 3450
Connection ~ 7400 3650
Wire Wire Line
	7400 3650 7500 3650
Connection ~ 7400 3350
Wire Wire Line
	7400 3350 7400 3250
Connection ~ 7400 3450
Wire Wire Line
	7400 3450 7400 3350
$Comp
L Device:C C?
U 1 1 5DBAC669
P 4500 4000
AR Path="/5981D798/5983DD90/5DBAC669" Ref="C?"  Part="1" 
AR Path="/5981D798/5983E4FD/5DBAC669" Ref="C?"  Part="1" 
AR Path="/59824968/5983F4B3/5DBAC669" Ref="C?"  Part="1" 
AR Path="/59824968/5983F7D4/5DBAC669" Ref="C?"  Part="1" 
AR Path="/5982788E/5983F4B3/5DBAC669" Ref="C?"  Part="1" 
AR Path="/5982788E/5983F7D4/5DBAC669" Ref="C?"  Part="1" 
AR Path="/59829749/59842ECB/5DBAC669" Ref="C?"  Part="1" 
AR Path="/59829749/59842FE8/5DBAC669" Ref="C?"  Part="1" 
AR Path="/59841F95/59842ECB/5DBAC669" Ref="C?"  Part="1" 
AR Path="/59841F95/59842FE8/5DBAC669" Ref="C?"  Part="1" 
AR Path="/59845D4E/59842ECB/5DBAC669" Ref="C?"  Part="1" 
AR Path="/59845D4E/59842FE8/5DBAC669" Ref="C?"  Part="1" 
AR Path="/59845ED6/59842ECB/5DBAC669" Ref="C?"  Part="1" 
AR Path="/59845ED6/59842FE8/5DBAC669" Ref="C?"  Part="1" 
AR Path="/59846013/59842ECB/5DBAC669" Ref="C?"  Part="1" 
AR Path="/59846013/59842FE8/5DBAC669" Ref="C?"  Part="1" 
AR Path="/5DBAC669" Ref="C?"  Part="1" 
AR Path="/5DBAA80F/5DBAC669" Ref="C?"  Part="1" 
AR Path="/5DBAE4F7/5DBAC669" Ref="C1"  Part="1" 
AR Path="/5DBAE549/5DBAC669" Ref="C3"  Part="1" 
F 0 "C1" H 4525 4100 50  0000 L CNN
F 1 "0.1uF" H 4525 3900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4538 3850 50  0000 C CNN
F 3 "" H 4500 4000 50  0000 C CNN
	1    4500 4000
	1    0    0    -1  
$EndComp
$EndSCHEMATC
